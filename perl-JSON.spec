%global rpm_version 4.10

Name:            perl-JSON
Version:         4.10
Release:         2
Summary:         JSON (JavaScript Object Notation) encoder/decoder
License:         GPL-1.0-or-later OR Artistic-1.0-Perl
URL:             https://metacpan.org/release/JSON
Source0:         https://cpan.metacpan.org/authors/id/I/IS/ISHIGAKI/JSON-%{rpm_version}.tar.gz

BuildRequires:   perl-generators perl perl(ExtUtils::MakeMaker) perl(Test::More)
BuildArch:       noarch

%{?perl_default_filter}
%global __provides_exclude %{?__provides_exclude:%__provides_exclude|}perl\\(JSON::(Backend::PP|backportPP::Boolean|Boolean|PP|PP::IncrParser)\\)
%global __requires_exclude %{?__requires_exclude:%__requires_exclude|}perl\\(JSON::(backportPP|backportPP::Boolean)\\)

%description
This module is a thin wrapper for JSON::XS-compatible modules with
a few additional features. All the backend modules convert a Perl
data structure to a JSON text and vice versa. This module uses
JSON::XS by default, and when JSON::XS is not available, falls back
on JSON::PP, which is in the Perl core since 5.14. If JSON::PP is
not available either, this module then falls back on JSON::backportPP
(which is actually JSON::PP in a different .pm file) bundled in the
same distribution as this module. You can also explicitly specify
to use Cpanel::JSON::XS, a fork of JSON::XS by Reini Urban.

%package_help

%prep
%autosetup -n JSON-%{rpm_version} -p1

%build
perl Makefile.PL NO_PACKLIST=1 INSTALLDIRS=vendor
%make_build

%install
make pure_install DESTDIR=$RPM_BUILD_ROOT
%{_fixperms} $RPM_BUILD_ROOT/*

%check
make test

%files
%doc README Changes
%{perl_vendorlib}/*.pm
%{perl_vendorlib}/JSON/*.pm
%{perl_vendorlib}/JSON/backportPP/*pm

%files help
%{_mandir}/man3/*3*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 4.10-2
- drop useless perl(:MODULE_COMPAT) requirement

* Mon May 15 2023 Ge Wang <wang__ge@126.com> - 4.10-1
- Upgrade to version 4.10

* Tue Jun 14 2022 SimpleUpdate Robot <tc@openeuler.org> - 4.06-1
- Upgrade to version 4.06

* Fri Nov 15 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.97.001-6
- Delete macro to solve build problem

* Wed Nov 13 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.97.001-5
- Package init

